#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>


struct ListNode {
    int val;
    std::shared_ptr<ListNode> next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

std::shared_ptr<ListNode> remove_back(std::shared_ptr<ListNode> head, int idx) {
    std::shared_ptr<ListNode> current = head;
    std::shared_ptr<ListNode> one_before;
    bool probably_first = false;
    for (int i = 0; i < idx; ++i) {
        current = current->next;
        if (!current) {
            probably_first = true;
            break;
        }
    }

    if (probably_first) {
        head = head->next;
        return head;
    }

    one_before = head;
    while (current->next) {
        current = current->next;
        one_before = one_before->next;
    }

    one_before->next = one_before->next->next;
    return head;
}

std::shared_ptr<ListNode> generate_list(std::vector<int> src) {
    if (src.size() < 1) { return nullptr; }
    std::shared_ptr<ListNode> head = std::make_shared<ListNode>(-1);
    std::shared_ptr<ListNode> current;
    for (const auto& val : src) {
        if (!current) {
            head->next = std::make_shared<ListNode>(val);
            current = head->next;
        } else {
            current->next = std::make_shared<ListNode>(val);
            current = current->next;
        }
    }
    return head->next;
}

std::string _str(std::shared_ptr<ListNode> head) {
    std::string ret = "[";
    std::shared_ptr current = head;
    while (current != nullptr) {
        ret += std::to_string(current->val);
        if (current->next) { ret += ", "; }
        current = current->next;
    }
    ret += "]";
    return ret;
}

int main() {
    using TestCase = std::tuple<
        std::shared_ptr<ListNode>,
        int,
        std::shared_ptr<ListNode>
    >;
    std::vector<TestCase> tests = {
        {generate_list({1, 2, 3, 4, 5}), 2, generate_list({1, 2, 3, 5})},
        {generate_list({1}), 1, generate_list({})},
        {generate_list({1, 2}), 1, generate_list({1})},
    };
    for (const auto& test : tests) {
        // Modifies the list in place, can't be bothered with copying.
        // Won't be able to check validity programmatically.
        std::cout << _str(std::get<0>(test))
            << ": " << _str(remove_back(std::get<0>(test), std::get<1>(test)))
            << " (" << _str(std::get<2>(test)) << ")" << std::endl;
    }
}