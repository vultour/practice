#include <iostream>
#include <string>
#include <vector>

bool is_palindrome(const std::string& s, int start, int end) {
    while ((start <= end) && (end >= start)) {
        if (s[start] != s[end]) { return false; }
        ++start;
        --end;
    }
    return true;
}

std::string find_longest(const std::string& s) {
    std::string longest = "";
    for (int i = 0; i < s.size(); ++i) {
        for (int j = s.size() - 1; j >= i; --j) {
            if (is_palindrome(s, i, j)) {
                std::string tmp = s.substr(i, (j - i + 1));
                if (tmp.size() > longest.size()) {
                    longest = tmp;
                    if (longest.size() >= (s.size() - i)) { return longest; }
                }
            }
        }
    }
    return longest;
}

int main() {
    using TestCase = std::pair<std::string, std::string>;
    std::vector<TestCase> tests = {
        TestCase("babad", "bab"),
        TestCase("cbbd", "bb"),
        TestCase("a", "a"),
        TestCase("ac", "a"),
    };
    for (const auto& test : tests) {
        std::string result = find_longest(test.first);
        std::cout << test.first << ": " << result
            << " (" << test.second << ")" << std::endl;
    }
    return 0;
}