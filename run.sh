#!/bin/sh
set -e

if [ -z "$1" ]; then
    echo "Usage: $0 <filename>" >&2
    exit 1
fi

BASE_DIR="$(dirname $0)"
binary_name="$(echo "$1" | grep -oE "[a-zA-Z]+_[0-9]+-" | tr -d "_-")"

mkdir -p "$BASE_DIR/bin"
clang++ -std=c++17 -Wall \
    -g -O0 -fsanitize=address \
    -o "$BASE_DIR/bin/$binary_name" \
    "$1"
$BASE_DIR/bin/$binary_name
