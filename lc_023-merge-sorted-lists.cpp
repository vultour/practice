#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>


struct ListNode {
    int val;
    std::shared_ptr<ListNode> next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};
using ListNodePtr = std::shared_ptr<ListNode>;

int find_lowest(std::vector<ListNodePtr> lists) {
    ListNodePtr lowest_ptr;
    int ret_index = -1;
    for (int i = 0; i < lists.size(); ++i) {
        if (lists[i]) {
            lowest_ptr = lists[i];
            ret_index = i;
            break;
        }
    }
    for (int i = 0; i < lists.size(); ++i) {
        if (lists[i]) {
            if (lists[i]->val < lowest_ptr->val) {
                lowest_ptr = lists[i];
                ret_index = i;
            }
        }
    }
    return ret_index;
}

ListNodePtr merge(std::vector<ListNodePtr> lists) {
    ListNodePtr head = std::make_shared<ListNode>(-1);
    ListNodePtr current = head;
    bool running = true;
    while (running) {
        int next = find_lowest(lists);
        if (next < 0) { running = false; }
        else {
            current->next = std::make_shared<ListNode>(lists[next]->val);
            current = current->next;
            lists[next] = lists[next]->next;
        }
    }
    return head->next;
}

ListNodePtr generate_list(std::vector<int> src) {
    if (src.size() < 1) { return nullptr; }
    ListNodePtr head = std::make_shared<ListNode>(-1);
    ListNodePtr current;
    for (const auto& val : src) {
        if (!current) {
            head->next = std::make_shared<ListNode>(val);
            current = head->next;
        } else {
            current->next = std::make_shared<ListNode>(val);
            current = current->next;
        }
    }
    return head->next;
}

std::string _str(ListNodePtr head) {
    std::string ret = "[";
    std::shared_ptr current = head;
    while (current != nullptr) {
        ret += std::to_string(current->val);
        if (current->next) { ret += ", "; }
        current = current->next;
    }
    ret += "]";
    return ret;
}

std::string __str(std::vector<ListNodePtr> lists) {
    std::string ret = "[";
    for (int i = 0; i < lists.size(); ++i) {
        ret += _str(lists[i]);
        if (i < (lists.size() - 1)) { ret += ", "; }
    }
    ret += "]";
    return ret;
}

int main() {
    using TestCase = std::tuple<
        std::vector<ListNodePtr>,
        std::shared_ptr<ListNode>
    >;
    std::vector<TestCase> tests = {
        {
            {
                generate_list({1, 4, 5}),
                generate_list({1, 3, 4}),
                generate_list({2, 6}),
            },
            generate_list({1, 1, 2, 3, 4, 4, 5, 6})
        },
        {{}, generate_list({})},
        {{generate_list({})}, generate_list({})}
    };
    for (const auto& test : tests) {
        // Modifies the list in place, can't be bothered with copying.
        // Won't be able to check validity programmatically.
        std::cout << __str(std::get<0>(test))
            << ": " << _str(merge(std::get<0>(test)))
            << " (" << _str(std::get<1>(test)) << ")" << std::endl;
    }
}