#include <queue>
#include <iostream>
#include <set>
#include <string>
#include <tuple>
#include <vector>


int length_of_longest_substr(std::string& s) {
    int longest = 0;
    std::queue<char> window;
    std::set<char> used;
    for (const auto& c : s) {
        if (used.find(c) == used.end()) {
            used.insert(c);
            window.push(c);
        } else {
            if (window.size() > longest) { longest = window.size(); }
            while (window.front() != c) {
                used.erase(window.front());
                window.pop();
            }
            window.pop();
            window.push(c);
        }
    }
    if (window.size() > longest) { longest = window.size(); }
    return longest;
}

int main() {
    using TestCase = std::tuple<std::string, int>;
    std::vector<TestCase> tests = {
        TestCase("abcabcbb", 3),
        TestCase("bbbbb", 1),
        TestCase("pwwkew", 3),
        TestCase(" ", 1),
        TestCase("dvdf", 3),
    };
    for (auto& tcase : tests) {
        int result = length_of_longest_substr(std::get<0>(tcase));
        std::cout << std::get<0>(tcase) << ": " << result
            << " (" << std::get<1>(tcase) << ")" << std::endl;
    }
    return 0;
}