#include <iostream>
#include <string>
#include <tuple>
#include <vector>

int find(std::string haystack, std::string needle) {
    if (needle.empty()) { return 0; }
    for (
        int i = 0;
        i < (
            static_cast<int>(haystack.size()) - static_cast<int>(needle.size())
            + 1
        );
        ++i
    ) {
        bool found = true;
        for (int j = 0; j < needle.size(); ++j) {
            if (haystack[i + j] != needle[j]) {
                found = false;
                break;
            }
        }
        if (found) { return i; }
    }
    return -1;
}

int main() {
    using TestCase = std::tuple<std::string, std::string, int>;
    std::vector<TestCase> tests = {
        TestCase("hello", "ll", 2),
        TestCase("aaaaa", "bba", -1),
        TestCase("a", "a", 0),
        TestCase("", "", 0),
        TestCase("", "a", -1),
    };
    for (const auto& test : tests) {
        int result = find(std::get<0>(test), std::get<1>(test));
        std::cout << std::get<0>(test) << ": " << result
            << " (" << std::get<2>(test) << ")" << std::endl;
    }
    return 0;
}