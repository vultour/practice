#include <array>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

/*
 * PAYPALISHIRING : 1
 * PAYPALISHIRING
 *
 * PAYPALISHIRING : 2 = 1s1, 2s1
 * PYAIHRN
 * APLSIIG
 *
 * PAYPALISHIRING : 3 = 1s3, 2s1, 3s3
 * P A H N
 * APLSIIG
 * Y I R
 *
 * PAYPALISHIRING : 4 = 1s5, 2s3, 2s1, 3s1, 3s3, 4s5
 * P  I  N
 * A LS IG
 * YA HR
 * P  I
 *
 * PAYPALISHIRING : 5 = 1s7, 2s5, 2s1, 3s3, 4s1, 4s5, 5s7
 * P   H
 * A  SI
 * Y I R
 * PL  IG
 * A   N
 *
 * PAYPALISHIRINGRIGHTNOW : 6 = 1s9, 2s7, 2s1, 3s5, 3s3, 4s3, 4s5, 5s1, 5s7, 6s9
 * P    R    O
 * A   II   NW
 * Y  H N  T
 * P S  G H
 * AI   RG
 * L    I
 *
 * PAYPALISHIRINGRIGHTNOW : 7
 * P     N
 * A    IG
 * Y   R R
 * P  I  I  W
 * A H   G O
 * LS    HN
 * I     T
 *
 * 1:  1 + ((rows - 2) * 2)
 * 2:  1 + ((rows - 3) * 2)
 *     1
 * 3:  1 + ((rows - 4) * 2)
 *     3
 * M:  (rows - 2)
 * -3: 3
 *     1 + ((rows - 4) * 2)
 * -2: 1
 *     1 + ((rows - 3) * 2)
 * -1: 1 + ((rows - 2) * 2)
 */

// Calculates the required skip for each row as described above
std::array<int, 2> calculate_skip(int rows, int index) {
    // Not skipping anything if there's only one row
    if (rows < 2) { return {0, 0}; }

    // Find midpoint
    bool is_even = ((rows % 2) == 0);
    int midpoint = (rows / 2);
    if (!is_even) { ++midpoint; }
    if ((!is_even) && (index == midpoint)) {
        // Special case at midpoint
        return {(rows - 2), (rows - 2)};
    }

    // Are we reversing?
    bool reverse = (index > midpoint);
    int relative_index = index;
    if (reverse) {
        // Turn indexes past the midpoint into their pre-midpoint counterparts
        relative_index = rows - (index - 1);
    }

    // Find if index is at either far end
    bool edge = ((index < 2) || (index >= rows));

    std::array<int, 2> possible;
    possible[0] = 1 + ((rows - (relative_index + 1)) * 2);
    possible[1] = 1 + ((relative_index - 2) * 2);
    if (edge) {
        // No alternate skip for first and last row
        possible[1] = possible[0];
    }
    if (reverse) {
        // Swap with alternate if we're past midpoint
        int tmp = possible[0];
        possible[0] = possible[1];
        possible[1] = tmp;
    }

    return possible;
}

std::string zigzag_string_flat(const std::string& s, int rows) {
    std::string result;
    result.reserve(s.size());
    for (int i = 0; i < rows; ++i) {
        // Abort if more rows were requested than available characters
        if (i >= s.size()) { break; }
        auto options = calculate_skip(rows, i + 1);
        int j = i;
        int x = 0; // Used to perform alternate skip every other iteration
        // Build the current row
        do {
            result += s[j];
            j += options[x % 2] + 1; // Skip and advance past
            ++x;
        } while (j < s.size());
    }
    return result;
}

int main() {
    using TestCase = std::tuple<std::string, int, std::string>;
    std::vector<TestCase> tests = {
        TestCase("PAYPALISHIRING", 1, "PAYPALISHIRING"),
        TestCase("PAYPALISHIRING", 2, "PYAIHRNAPLSIIG"),
        TestCase("PAYPALISHIRING", 3, "PAHNAPLSIIGYIR"),
        TestCase("PAYPALISHIRING", 4, "PINALSIGYAHRPI"),
        TestCase("PAYPALISHIRING", 5, "PHASIYIRPLIGAN"),
        TestCase("PAYPALISHIRINGRIGHTNOW", 6, "PROAIINWYHNTPSGHAIRGLI"),
        TestCase("PAYPALISHIRINGRIGHTNOW", 7, "PNAIGYRRPIIWAHGOLSHNIT"),
        TestCase("A", 1, "A"),
        TestCase("A", 2, "A"),
    };
    for (const auto& test : tests) {
        std::string result = zigzag_string_flat(
            std::get<0>(test),
            std::get<1>(test)
        );
        std::cout << std::get<0>(test) << ": " << result
            << " (" << std::get<2>(test) << ")"
            << " - " << ((result == std::get<2>(test)) ? ("OK") : ("FAIL"))
            << std::endl;
    }
    return 0;
}