#include <array>
#include <iostream>
#include <vector>

// Finds the median of the two sorted arrays
double find_median(const std::vector<int>& a, const std::vector<int>& b) {
    int index_a = 0;
    int index_b = 0;
    if (a.size() < 1) { index_a = 1; }
    if (b.size() < 1) { index_b = 1; }

    bool is_even = (((a.size() + b.size()) % 2) == 0);
    int midpoint = ((a.size() + b.size()) / 2);

    std::array<int, 2> cache;
    for (int i = 0; i <= midpoint; ++i) {
        if (index_a < a.size()) {
            if (index_b < b.size()) {
                if (a[index_a] <= b[index_b]) {
                    std::cout << a[index_a] << std::endl;
                    cache[i % 2] = a[index_a];
                    ++index_a;
                } else {
                    std::cout << b[index_b] << std::endl;
                    cache[i % 2] = b[index_b];
                    ++index_b;
                }
            } else {
                std::cout << a[index_a] << std::endl;
                cache[i % 2] = a[index_a];
                ++index_a;
            }
        } else if (index_b < b.size()) {
            if (index_a < a.size()) {
                if (b[index_b] <= a[index_a]) {
                    std::cout << b[index_b] << std::endl;
                    cache[i % 2] = b[index_b];
                    ++index_b;
                } else {
                    std::cout << a[index_a] << std::endl;
                    cache[i % 2] = a[index_a];
                    ++index_a;
                }
            } else {
                std::cout << b[index_b] << std::endl;
                cache[i % 2] = b[index_b];
                ++index_b;
            }
        } else {
            std::cout << "<impossible>" << std::endl;
        }
    }

    if (is_even) { return ((cache[0] + cache[1]) / 2.0); }
    else { return cache[midpoint % 2]; }
}

int main() {
    using InputVec = std::vector<int>;
    using TestCase = std::tuple<InputVec, InputVec, double>;
    std::vector<TestCase> tests = {
        TestCase({1, 3}, {2}, 2.0),
        TestCase({1, 2}, {3, 4}, 2.5),
        TestCase({0, 0}, {0, 0}, 0.0),
        TestCase({}, {1}, 1.0),
        TestCase({2}, {}, 2.0),
    };
    int i = 1;
    for (const auto& tcase : tests) {
        double result = find_median(std::get<0>(tcase), std::get<1>(tcase));
        std::cout << "Test " << i << ": " << result
            << " (" << std::get<2>(tcase) << ")" << std::endl;
        ++i;
    }
    return 0;
}