#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <vector>


static std::map<char, std::vector<char>> possibilities = {
    {'2', {'a', 'b', 'c'}},
    {'3', {'d', 'e', 'f'}},
    {'4', {'g', 'h', 'i'}},
    {'5', {'j', 'k', 'l'}},
    {'6', {'m', 'n', 'o'}},
    {'7', {'p', 'q', 'r', 's'}},
    {'8', {'t', 'u', 'v'}},
    {'9', {'w', 'x', 'y', 'z'}},
};

std::vector<std::string> combination(std::string digits) {
    if (digits.size() < 1) { return {}; }
    std::vector<std::string> ret;
    ret.reserve(4);
    for (const auto& c : possibilities[digits[0]]) {
        std::vector<std::string> sub_combs = {""};
        if (digits.size() > 1) {
            sub_combs = combination(digits.substr(1, std::string::npos));
        }
        for (const auto& sub : sub_combs) {
            ret.push_back(c + sub);
        }
    }
    return ret;
}

std::string _str(const std::vector<std::string>& vec) {
    std::string ret = "[";
    for (int i = 0; i < vec.size(); ++i) {
        ret += "'";
        ret += vec[i];
        ret += "'";
        if (i < (vec.size() - 1)) { ret += ", "; }
    }
    ret += "]";
    return ret;
}

int main() {
    using TestCase = std::tuple<std::string, std::vector<std::string>>;
    std::vector<TestCase> tests = {
        {"23", {"ad","ae","af","bd","be","bf","cd","ce","cf"}},
        {"", {}},
        {"2", {"a","b","c"}},
    };
    for (const auto& test : tests) {
        auto result = combination(std::get<0>(test));
        std::cout << std::get<0>(test) << ": " << _str(result)
            << " (" << _str(std::get<1>(test)) << ")"
            << " - " << ((result == std::get<1>(test)) ? ("OK") : ("FAIL"))
            << std::endl;
    }
}